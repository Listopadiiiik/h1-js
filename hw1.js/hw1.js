// Теоретичні питання

// 1. Оголосити змінну можна через let, const або var(старий метод);

// 2. prompt запитує у користувача ввести щось у поле та підтвердити.
// confirm просить у користувача лише натиснути "OK" або "cancel(відміна)".

// 3. Перетворення типів це своєрідна конвертація данних у якийсь тип.
// Але буває так, що вираз записується зі значеннями різних типів. Приклад:
// let f;
// f = example0991 (тут 2 типи: string та number)
// або 
// let age;
// age = null false; (а тут null та boolean)


// Завдання

// 1.
// let name = "Vlad";  
// let admin = name;
// console.log(admin);

// 2.
// let days = 9;
// console.log(days * 24 * 60 * 60 + ' seconds');

// 3.
// let message;
// message = prompt("Write anything");
// console.log(message);